load('mnist_patches.mat');
% colormap(1-gray);
% centers8 = streaming_kmeans(patches,8);
% centers16 = streaming_kmeans(patches,16);
% centers32 = streaming_kmeans(patches,32);
% centers64 = streaming_kmeans(patches,64);
% 
% image24_8 = decode_qimage(quantize_image(testdata(:,:,24),centers8),centers8);
% image24_64 = decode_qimage(quantize_image(testdata(:,:,24),centers64),centers64);
% imagesc([testdata(:,:,24), image24_8,image24_64]);
% 
%  colormap(1-gray);
% image100_8 = decode_qimage(quantize_image(testdata(:,:,100),centers8),centers8);
% image100_64 = decode_qimage(quantize_image(testdata(:,:,100),centers64),centers64);
% imagesc([testdata(:,:,100), image100_8,image100_64]);
% 
% colormap(1-gray);
% image5000_8 = decode_qimage(quantize_image(testdata(:,:,5000),centers8),centers8);
% image5000_64 = decode_qimage(quantize_image(testdata(:,:,5000),centers64),centers64);
% imagesc([testdata(:,:,5000), image5000_8, image5000_64]);
ks = [8,16,32,64];
y2 = cost(ks);
y1 = zeros(1,length(ks));
for i=1:length(ks)
    y1(1,i) = log2(ks(i));
end
figure
plot(y1,y2)

