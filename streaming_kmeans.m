function centers = streaming_kmeans( patches, k )
    centers = zeros(k, size(patches,2));
    n = ones(1,k);
    for i=1:k
        centers(i,:) = patches(i,:);
    end
    for i=k+1:length(patches)
        %curr_pat = patches(i,:);
        %tmp = repmat(patches(1,:),k,1);
        x = patches(i,:);
        diffs = bsxfun(@minus,x,centers);
        j_all = zeros(k,1);
        for q=1:length(j_all)
            j_all(q) = diffs(q,:)*diffs(q,:)';
        end
        [~,j] = min(j_all);
        %j = knnsearch(centers, patches(i,:));
        n(1,j) = n(1,j)+1;
        recip = 1/n(j);
        centers(j,:) = ((1-recip)*centers(j,:)) + (recip*patches(i,:));
    end
end

