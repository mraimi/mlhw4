function costs = cost(ks)
    load('ctrs8');
    load('ctrs16');
    load('ctrs32');
    load('ctrs64');
    load('mnist_patches.mat');
    costs = zeros(1,length(ks));
    c = cell(4,1);
    c{1} = centers8;
    c{2} = centers16;
    c{3} = centers32;
    c{4} = centers64;
    for i=1:length(ks)
       total = 0;
       for x=1:length(testpatches)
            curr_patch = testpatches(x,:);
            centers = c{i};
            diffs = bsxfun(@minus,curr_patch,centers);
            j_all = zeros(ks(1,i),1);
            for q=1:length(j_all)
                j_all(q) = diffs(q,:)*diffs(q,:)';
            end
            [~,j] = min(j_all);
            total = total + j_all(j,1);
       end
       costs(1,i) = total;
    end
end

